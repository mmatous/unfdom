
const galleryElements = document.body.querySelectorAll('a.image.lightbox')
for (const elem of galleryElements) {
	const thumb = elem.querySelector('img.thumbimage.lazyload')
	const src = thumb.getAttribute('data-src')
	const needle = '/latest'
	const fullImage = src.substring(0, src.indexOf(needle) + needle.length)
	elem.setAttribute('href', fullImage)
}
