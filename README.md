# unfdom

Fix *.fandom.com gallery browsing.

Fandom's lightbox unnecessarily crops
images even on high enough screens while middle-click and opening in a new tab
redirect back to the page without opening the image.
Unfdom changes the latter and makes images open in a new tab.

## Build

```
web-ext build
web-ext sign --api-key=<API_KEY> --api-secret=<API_SECRET>
```

Both key and secret can be found after registering at http://addons.mozilla.org/.

## License

[The Unlicense](LICENSE)
